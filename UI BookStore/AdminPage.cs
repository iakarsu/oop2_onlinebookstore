﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI_BookStore
{
    public partial class AdminPage : Form
    {
        public AdminPage()
        {
            InitializeComponent();
        }

        AddBook addBook = new AddBook();
        AddMagazine addMagazine = new AddMagazine();
        AddCD addCD = new AddCD();

        private void AdminPage_Load(object sender, EventArgs e)
        {
            
        }

        private void button_ChangePP_Click(object sender, EventArgs e)
        {
            string imagelocation;
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.ShowDialog();

            imagelocation = ofd.FileName;
            pictureBox_adminPP.ImageLocation = imagelocation;
            
        }

        private void button_addBook_Click(object sender, EventArgs e)
        {
            addBook.ShowDialog();
        }

        private void button_addMag_Click(object sender, EventArgs e)
        {
            addMagazine.ShowDialog();
        }

        private void button_addCd_Click(object sender, EventArgs e)
        {
            addCD.ShowDialog();
        }

        private void button_UserControl_Click(object sender, EventArgs e)
        {

        }

        

        
    }
}
