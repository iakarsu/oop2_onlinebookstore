﻿namespace UI_BookStore
{
    partial class AdminPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminPage));
            this.button_addBook = new System.Windows.Forms.Button();
            this.button_addMag = new System.Windows.Forms.Button();
            this.button_addCd = new System.Windows.Forms.Button();
            this.button_UserControl = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox_adminPP = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label_adminname = new System.Windows.Forms.Label();
            this.button_ChangePP = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_adminPP)).BeginInit();
            this.SuspendLayout();
            // 
            // button_addBook
            // 
            this.button_addBook.BackColor = System.Drawing.Color.LightSeaGreen;
            this.button_addBook.FlatAppearance.BorderSize = 0;
            this.button_addBook.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_addBook.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_addBook.ForeColor = System.Drawing.Color.White;
            this.button_addBook.Location = new System.Drawing.Point(0, 5);
            this.button_addBook.Name = "button_addBook";
            this.button_addBook.Size = new System.Drawing.Size(200, 100);
            this.button_addBook.TabIndex = 0;
            this.button_addBook.Text = "Add Book";
            this.button_addBook.UseVisualStyleBackColor = false;
            this.button_addBook.Click += new System.EventHandler(this.button_addBook_Click);
            // 
            // button_addMag
            // 
            this.button_addMag.BackColor = System.Drawing.Color.LightSeaGreen;
            this.button_addMag.FlatAppearance.BorderSize = 0;
            this.button_addMag.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_addMag.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_addMag.ForeColor = System.Drawing.Color.White;
            this.button_addMag.Location = new System.Drawing.Point(0, 111);
            this.button_addMag.Name = "button_addMag";
            this.button_addMag.Size = new System.Drawing.Size(200, 100);
            this.button_addMag.TabIndex = 1;
            this.button_addMag.Text = "Add Magazine";
            this.button_addMag.UseVisualStyleBackColor = false;
            this.button_addMag.Click += new System.EventHandler(this.button_addMag_Click);
            // 
            // button_addCd
            // 
            this.button_addCd.BackColor = System.Drawing.Color.LightSeaGreen;
            this.button_addCd.FlatAppearance.BorderSize = 0;
            this.button_addCd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_addCd.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_addCd.ForeColor = System.Drawing.Color.White;
            this.button_addCd.Location = new System.Drawing.Point(3, 215);
            this.button_addCd.Name = "button_addCd";
            this.button_addCd.Size = new System.Drawing.Size(200, 100);
            this.button_addCd.TabIndex = 2;
            this.button_addCd.Text = "Add Music CD";
            this.button_addCd.UseVisualStyleBackColor = false;
            this.button_addCd.Click += new System.EventHandler(this.button_addCd_Click);
            // 
            // button_UserControl
            // 
            this.button_UserControl.BackColor = System.Drawing.Color.LightSeaGreen;
            this.button_UserControl.FlatAppearance.BorderSize = 0;
            this.button_UserControl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_UserControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_UserControl.ForeColor = System.Drawing.Color.White;
            this.button_UserControl.Location = new System.Drawing.Point(0, 304);
            this.button_UserControl.Name = "button_UserControl";
            this.button_UserControl.Size = new System.Drawing.Size(200, 108);
            this.button_UserControl.TabIndex = 3;
            this.button_UserControl.Text = "User Control";
            this.button_UserControl.UseVisualStyleBackColor = false;
            this.button_UserControl.Click += new System.EventHandler(this.button_UserControl_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightSeaGreen;
            this.panel1.Controls.Add(this.button_addBook);
            this.panel1.Controls.Add(this.button_UserControl);
            this.panel1.Controls.Add(this.button_addMag);
            this.panel1.Controls.Add(this.button_addCd);
            this.panel1.Location = new System.Drawing.Point(-3, -4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 560);
            this.panel1.TabIndex = 4;
            // 
            // pictureBox_adminPP
            // 
            this.pictureBox_adminPP.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_adminPP.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_adminPP.Image")));
            this.pictureBox_adminPP.Location = new System.Drawing.Point(306, 107);
            this.pictureBox_adminPP.Name = "pictureBox_adminPP";
            this.pictureBox_adminPP.Size = new System.Drawing.Size(216, 253);
            this.pictureBox_adminPP.TabIndex = 5;
            this.pictureBox_adminPP.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.label1.Location = new System.Drawing.Point(436, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(246, 55);
            this.label1.TabIndex = 6;
            this.label1.Text = "My Profile";
            // 
            // label_adminname
            // 
            this.label_adminname.AutoSize = true;
            this.label_adminname.BackColor = System.Drawing.Color.Transparent;
            this.label_adminname.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_adminname.Location = new System.Drawing.Point(582, 107);
            this.label_adminname.Name = "label_adminname";
            this.label_adminname.Size = new System.Drawing.Size(299, 29);
            this.label_adminname.TabIndex = 7;
            this.label_adminname.Text = "Welcome Admin_Name !";
            // 
            // button_ChangePP
            // 
            this.button_ChangePP.BackColor = System.Drawing.Color.LightSeaGreen;
            this.button_ChangePP.FlatAppearance.BorderSize = 0;
            this.button_ChangePP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_ChangePP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_ChangePP.ForeColor = System.Drawing.Color.White;
            this.button_ChangePP.Location = new System.Drawing.Point(306, 366);
            this.button_ChangePP.Name = "button_ChangePP";
            this.button_ChangePP.Size = new System.Drawing.Size(216, 28);
            this.button_ChangePP.TabIndex = 9;
            this.button_ChangePP.Text = "Change profile picture";
            this.button_ChangePP.UseVisualStyleBackColor = false;
            this.button_ChangePP.Click += new System.EventHandler(this.button_ChangePP_Click);
            // 
            // AdminPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(979, 517);
            this.Controls.Add(this.button_ChangePP);
            this.Controls.Add(this.label_adminname);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox_adminPP);
            this.Controls.Add(this.panel1);
            this.Name = "AdminPage";
            this.Text = "AdminPage";
            this.Load += new System.EventHandler(this.AdminPage_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_adminPP)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_addBook;
        private System.Windows.Forms.Button button_addMag;
        private System.Windows.Forms.Button button_addCd;
        private System.Windows.Forms.Button button_UserControl;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox_adminPP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_adminname;
        private System.Windows.Forms.Button button_ChangePP;
    }
}