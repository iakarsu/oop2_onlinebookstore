﻿namespace UI_BookStore
{
    partial class AddBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddBook));
            this.textBox_BOOKname = new System.Windows.Forms.TextBox();
            this.textBox_BOOKauthor = new System.Windows.Forms.TextBox();
            this.textBox_BOOKpublisher = new System.Windows.Forms.TextBox();
            this.textBox_BOOKpage = new System.Windows.Forms.TextBox();
            this.textBox_BOOKisbn = new System.Windows.Forms.TextBox();
            this.pictureBox_BOOKcover = new System.Windows.Forms.PictureBox();
            this.label_name = new System.Windows.Forms.Label();
            this.label_author = new System.Windows.Forms.Label();
            this.label_publisher = new System.Windows.Forms.Label();
            this.label_page = new System.Windows.Forms.Label();
            this.label_isbn = new System.Windows.Forms.Label();
            this.button_addcover = new System.Windows.Forms.Button();
            this.button_AddBook = new System.Windows.Forms.Button();
            this.label_price = new System.Windows.Forms.Label();
            this.textBox_BOOKprice = new System.Windows.Forms.TextBox();
            this.label_BOOKdesc = new System.Windows.Forms.Label();
            this.textBox_BOOKdesc = new System.Windows.Forms.TextBox();
            this.label_sale = new System.Windows.Forms.Label();
            this.textBox_BOOKsale = new System.Windows.Forms.TextBox();
            this.label_type = new System.Windows.Forms.Label();
            this.textBox_BOOKtype = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_BOOKcover)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox_BOOKname
            // 
            this.textBox_BOOKname.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_BOOKname.Location = new System.Drawing.Point(145, 47);
            this.textBox_BOOKname.Name = "textBox_BOOKname";
            this.textBox_BOOKname.Size = new System.Drawing.Size(219, 35);
            this.textBox_BOOKname.TabIndex = 0;
            // 
            // textBox_BOOKauthor
            // 
            this.textBox_BOOKauthor.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_BOOKauthor.Location = new System.Drawing.Point(145, 84);
            this.textBox_BOOKauthor.Name = "textBox_BOOKauthor";
            this.textBox_BOOKauthor.Size = new System.Drawing.Size(219, 35);
            this.textBox_BOOKauthor.TabIndex = 1;
            // 
            // textBox_BOOKpublisher
            // 
            this.textBox_BOOKpublisher.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_BOOKpublisher.Location = new System.Drawing.Point(145, 121);
            this.textBox_BOOKpublisher.Name = "textBox_BOOKpublisher";
            this.textBox_BOOKpublisher.Size = new System.Drawing.Size(219, 35);
            this.textBox_BOOKpublisher.TabIndex = 2;
            // 
            // textBox_BOOKpage
            // 
            this.textBox_BOOKpage.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_BOOKpage.Location = new System.Drawing.Point(145, 157);
            this.textBox_BOOKpage.Name = "textBox_BOOKpage";
            this.textBox_BOOKpage.Size = new System.Drawing.Size(219, 35);
            this.textBox_BOOKpage.TabIndex = 3;
            // 
            // textBox_BOOKisbn
            // 
            this.textBox_BOOKisbn.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_BOOKisbn.Location = new System.Drawing.Point(145, 194);
            this.textBox_BOOKisbn.Name = "textBox_BOOKisbn";
            this.textBox_BOOKisbn.Size = new System.Drawing.Size(219, 35);
            this.textBox_BOOKisbn.TabIndex = 4;
            // 
            // pictureBox_BOOKcover
            // 
            this.pictureBox_BOOKcover.Location = new System.Drawing.Point(447, 47);
            this.pictureBox_BOOKcover.Name = "pictureBox_BOOKcover";
            this.pictureBox_BOOKcover.Size = new System.Drawing.Size(246, 219);
            this.pictureBox_BOOKcover.TabIndex = 5;
            this.pictureBox_BOOKcover.TabStop = false;
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.BackColor = System.Drawing.Color.Transparent;
            this.label_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_name.Location = new System.Drawing.Point(74, 54);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(65, 24);
            this.label_name.TabIndex = 6;
            this.label_name.Text = "Name";
            // 
            // label_author
            // 
            this.label_author.AutoSize = true;
            this.label_author.BackColor = System.Drawing.Color.Transparent;
            this.label_author.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_author.Location = new System.Drawing.Point(67, 91);
            this.label_author.Name = "label_author";
            this.label_author.Size = new System.Drawing.Size(72, 24);
            this.label_author.TabIndex = 7;
            this.label_author.Text = "Author";
            // 
            // label_publisher
            // 
            this.label_publisher.AutoSize = true;
            this.label_publisher.BackColor = System.Drawing.Color.Transparent;
            this.label_publisher.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_publisher.Location = new System.Drawing.Point(41, 128);
            this.label_publisher.Name = "label_publisher";
            this.label_publisher.Size = new System.Drawing.Size(98, 24);
            this.label_publisher.TabIndex = 8;
            this.label_publisher.Text = "Publisher";
            // 
            // label_page
            // 
            this.label_page.AutoSize = true;
            this.label_page.BackColor = System.Drawing.Color.Transparent;
            this.label_page.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_page.Location = new System.Drawing.Point(81, 164);
            this.label_page.Name = "label_page";
            this.label_page.Size = new System.Drawing.Size(58, 24);
            this.label_page.TabIndex = 9;
            this.label_page.Text = "Page";
            // 
            // label_isbn
            // 
            this.label_isbn.AutoSize = true;
            this.label_isbn.BackColor = System.Drawing.Color.Transparent;
            this.label_isbn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_isbn.Location = new System.Drawing.Point(83, 201);
            this.label_isbn.Name = "label_isbn";
            this.label_isbn.Size = new System.Drawing.Size(56, 24);
            this.label_isbn.TabIndex = 10;
            this.label_isbn.Text = "ISBN";
            // 
            // button_addcover
            // 
            this.button_addcover.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_addcover.Location = new System.Drawing.Point(443, 276);
            this.button_addcover.Name = "button_addcover";
            this.button_addcover.Size = new System.Drawing.Size(250, 40);
            this.button_addcover.TabIndex = 11;
            this.button_addcover.Text = "Add Book Cover";
            this.button_addcover.UseVisualStyleBackColor = true;
            this.button_addcover.Click += new System.EventHandler(this.button_addcover_Click);
            // 
            // button_AddBook
            // 
            this.button_AddBook.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_AddBook.Location = new System.Drawing.Point(648, 368);
            this.button_AddBook.Name = "button_AddBook";
            this.button_AddBook.Size = new System.Drawing.Size(140, 70);
            this.button_AddBook.TabIndex = 12;
            this.button_AddBook.Text = "Add Book";
            this.button_AddBook.UseVisualStyleBackColor = true;
            this.button_AddBook.Click += new System.EventHandler(this.button_AddBook_Click);
            // 
            // label_price
            // 
            this.label_price.AutoSize = true;
            this.label_price.BackColor = System.Drawing.Color.Transparent;
            this.label_price.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_price.Location = new System.Drawing.Point(81, 238);
            this.label_price.Name = "label_price";
            this.label_price.Size = new System.Drawing.Size(58, 24);
            this.label_price.TabIndex = 14;
            this.label_price.Text = "Price";
            // 
            // textBox_BOOKprice
            // 
            this.textBox_BOOKprice.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_BOOKprice.Location = new System.Drawing.Point(145, 231);
            this.textBox_BOOKprice.Name = "textBox_BOOKprice";
            this.textBox_BOOKprice.Size = new System.Drawing.Size(219, 35);
            this.textBox_BOOKprice.TabIndex = 13;
            // 
            // label_BOOKdesc
            // 
            this.label_BOOKdesc.AutoSize = true;
            this.label_BOOKdesc.BackColor = System.Drawing.Color.Transparent;
            this.label_BOOKdesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_BOOKdesc.Location = new System.Drawing.Point(24, 350);
            this.label_BOOKdesc.Name = "label_BOOKdesc";
            this.label_BOOKdesc.Size = new System.Drawing.Size(115, 24);
            this.label_BOOKdesc.TabIndex = 16;
            this.label_BOOKdesc.Text = "Description";
            // 
            // textBox_BOOKdesc
            // 
            this.textBox_BOOKdesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_BOOKdesc.Location = new System.Drawing.Point(145, 343);
            this.textBox_BOOKdesc.Multiline = true;
            this.textBox_BOOKdesc.Name = "textBox_BOOKdesc";
            this.textBox_BOOKdesc.Size = new System.Drawing.Size(219, 65);
            this.textBox_BOOKdesc.TabIndex = 15;
            // 
            // label_sale
            // 
            this.label_sale.AutoSize = true;
            this.label_sale.BackColor = System.Drawing.Color.Transparent;
            this.label_sale.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_sale.Location = new System.Drawing.Point(81, 276);
            this.label_sale.Name = "label_sale";
            this.label_sale.Size = new System.Drawing.Size(51, 24);
            this.label_sale.TabIndex = 18;
            this.label_sale.Text = "Sale";
            // 
            // textBox_BOOKsale
            // 
            this.textBox_BOOKsale.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_BOOKsale.Location = new System.Drawing.Point(145, 269);
            this.textBox_BOOKsale.Name = "textBox_BOOKsale";
            this.textBox_BOOKsale.Size = new System.Drawing.Size(219, 35);
            this.textBox_BOOKsale.TabIndex = 17;
            // 
            // label_type
            // 
            this.label_type.AutoSize = true;
            this.label_type.BackColor = System.Drawing.Color.Transparent;
            this.label_type.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_type.Location = new System.Drawing.Point(81, 313);
            this.label_type.Name = "label_type";
            this.label_type.Size = new System.Drawing.Size(57, 24);
            this.label_type.TabIndex = 20;
            this.label_type.Text = "Type";
            // 
            // textBox_BOOKtype
            // 
            this.textBox_BOOKtype.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_BOOKtype.Location = new System.Drawing.Point(145, 306);
            this.textBox_BOOKtype.Name = "textBox_BOOKtype";
            this.textBox_BOOKtype.Size = new System.Drawing.Size(219, 35);
            this.textBox_BOOKtype.TabIndex = 19;
            // 
            // AddBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label_type);
            this.Controls.Add(this.textBox_BOOKtype);
            this.Controls.Add(this.label_sale);
            this.Controls.Add(this.textBox_BOOKsale);
            this.Controls.Add(this.label_BOOKdesc);
            this.Controls.Add(this.textBox_BOOKdesc);
            this.Controls.Add(this.label_price);
            this.Controls.Add(this.textBox_BOOKprice);
            this.Controls.Add(this.button_AddBook);
            this.Controls.Add(this.button_addcover);
            this.Controls.Add(this.label_isbn);
            this.Controls.Add(this.label_page);
            this.Controls.Add(this.label_publisher);
            this.Controls.Add(this.label_author);
            this.Controls.Add(this.label_name);
            this.Controls.Add(this.pictureBox_BOOKcover);
            this.Controls.Add(this.textBox_BOOKisbn);
            this.Controls.Add(this.textBox_BOOKpage);
            this.Controls.Add(this.textBox_BOOKpublisher);
            this.Controls.Add(this.textBox_BOOKauthor);
            this.Controls.Add(this.textBox_BOOKname);
            this.Name = "AddBook";
            this.Text = "AddBook";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_BOOKcover)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_BOOKname;
        private System.Windows.Forms.TextBox textBox_BOOKauthor;
        private System.Windows.Forms.TextBox textBox_BOOKpublisher;
        private System.Windows.Forms.TextBox textBox_BOOKpage;
        private System.Windows.Forms.TextBox textBox_BOOKisbn;
        private System.Windows.Forms.PictureBox pictureBox_BOOKcover;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.Label label_author;
        private System.Windows.Forms.Label label_publisher;
        private System.Windows.Forms.Label label_page;
        private System.Windows.Forms.Label label_isbn;
        private System.Windows.Forms.Button button_addcover;
        private System.Windows.Forms.Button button_AddBook;
        private System.Windows.Forms.Label label_price;
        private System.Windows.Forms.TextBox textBox_BOOKprice;
        private System.Windows.Forms.Label label_BOOKdesc;
        private System.Windows.Forms.TextBox textBox_BOOKdesc;
        private System.Windows.Forms.Label label_sale;
        private System.Windows.Forms.TextBox textBox_BOOKsale;
        private System.Windows.Forms.Label label_type;
        private System.Windows.Forms.TextBox textBox_BOOKtype;
    }
}