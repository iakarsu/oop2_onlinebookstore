﻿namespace UI_BookStore
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dashboard));
            this.panel1 = new System.Windows.Forms.Panel();
            this.bttnInvoice = new System.Windows.Forms.Button();
            this.bttnShoppingCart = new System.Windows.Forms.Button();
            this.bttnProfile = new System.Windows.Forms.Button();
            this.bttnHome = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lbBookStore = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lbWelcome = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lbTime = new System.Windows.Forms.Label();
            this.lblUserRole = new System.Windows.Forms.Label();
            this.Username = new System.Windows.Forms.Label();
            this.lbRole = new System.Windows.Forms.Label();
            this.timerTime = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightSeaGreen;
            this.panel1.Controls.Add(this.bttnInvoice);
            this.panel1.Controls.Add(this.bttnShoppingCart);
            this.panel1.Controls.Add(this.bttnProfile);
            this.panel1.Controls.Add(this.bttnHome);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(215, 681);
            this.panel1.TabIndex = 0;
            // 
            // bttnInvoice
            // 
            this.bttnInvoice.FlatAppearance.BorderSize = 0;
            this.bttnInvoice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnInvoice.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bttnInvoice.ForeColor = System.Drawing.Color.White;
            this.bttnInvoice.Image = ((System.Drawing.Image)(resources.GetObject("bttnInvoice.Image")));
            this.bttnInvoice.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnInvoice.Location = new System.Drawing.Point(0, 324);
            this.bttnInvoice.Name = "bttnInvoice";
            this.bttnInvoice.Size = new System.Drawing.Size(215, 60);
            this.bttnInvoice.TabIndex = 6;
            this.bttnInvoice.Text = "Invoice";
            this.bttnInvoice.UseVisualStyleBackColor = true;
            // 
            // bttnShoppingCart
            // 
            this.bttnShoppingCart.FlatAppearance.BorderSize = 0;
            this.bttnShoppingCart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnShoppingCart.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bttnShoppingCart.ForeColor = System.Drawing.Color.White;
            this.bttnShoppingCart.Image = ((System.Drawing.Image)(resources.GetObject("bttnShoppingCart.Image")));
            this.bttnShoppingCart.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnShoppingCart.Location = new System.Drawing.Point(0, 262);
            this.bttnShoppingCart.Name = "bttnShoppingCart";
            this.bttnShoppingCart.Size = new System.Drawing.Size(215, 60);
            this.bttnShoppingCart.TabIndex = 5;
            this.bttnShoppingCart.Text = "        Shopping Cart";
            this.bttnShoppingCart.UseVisualStyleBackColor = true;
            // 
            // bttnProfile
            // 
            this.bttnProfile.FlatAppearance.BorderSize = 0;
            this.bttnProfile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnProfile.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bttnProfile.ForeColor = System.Drawing.Color.White;
            this.bttnProfile.Image = ((System.Drawing.Image)(resources.GetObject("bttnProfile.Image")));
            this.bttnProfile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnProfile.Location = new System.Drawing.Point(0, 200);
            this.bttnProfile.Name = "bttnProfile";
            this.bttnProfile.Size = new System.Drawing.Size(215, 60);
            this.bttnProfile.TabIndex = 4;
            this.bttnProfile.Text = "Profile";
            this.bttnProfile.UseVisualStyleBackColor = true;
            // 
            // bttnHome
            // 
            this.bttnHome.FlatAppearance.BorderSize = 0;
            this.bttnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnHome.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bttnHome.ForeColor = System.Drawing.Color.White;
            this.bttnHome.Image = ((System.Drawing.Image)(resources.GetObject("bttnHome.Image")));
            this.bttnHome.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnHome.Location = new System.Drawing.Point(0, 140);
            this.bttnHome.Name = "bttnHome";
            this.bttnHome.Size = new System.Drawing.Size(215, 60);
            this.bttnHome.TabIndex = 3;
            this.bttnHome.Text = "Home";
            this.bttnHome.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lbBookStore);
            this.panel4.Controls.Add(this.pictureBox1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(215, 140);
            this.panel4.TabIndex = 3;
            // 
            // lbBookStore
            // 
            this.lbBookStore.AutoSize = true;
            this.lbBookStore.ForeColor = System.Drawing.Color.White;
            this.lbBookStore.Location = new System.Drawing.Point(25, 94);
            this.lbBookStore.Name = "lbBookStore";
            this.lbBookStore.Size = new System.Drawing.Size(170, 22);
            this.lbBookStore.TabIndex = 1;
            this.lbBookStore.Text = "Online Book Store";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(50, -5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(112, 121);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lbWelcome
            // 
            this.lbWelcome.AutoSize = true;
            this.lbWelcome.ForeColor = System.Drawing.Color.White;
            this.lbWelcome.Location = new System.Drawing.Point(22, 9);
            this.lbWelcome.Name = "lbWelcome";
            this.lbWelcome.Size = new System.Drawing.Size(100, 22);
            this.lbWelcome.TabIndex = 2;
            this.lbWelcome.Text = "Welcome:";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.LightSeaGreen;
            this.panel3.Controls.Add(this.lbTime);
            this.panel3.Controls.Add(this.lblUserRole);
            this.panel3.Controls.Add(this.Username);
            this.panel3.Controls.Add(this.lbRole);
            this.panel3.Controls.Add(this.lbWelcome);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(215, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(969, 65);
            this.panel3.TabIndex = 2;
            // 
            // lbTime
            // 
            this.lbTime.AutoSize = true;
            this.lbTime.ForeColor = System.Drawing.Color.White;
            this.lbTime.Location = new System.Drawing.Point(882, 21);
            this.lbTime.Name = "lbTime";
            this.lbTime.Size = new System.Drawing.Size(75, 22);
            this.lbTime.TabIndex = 6;
            this.lbTime.Text = "HH:MM";
            // 
            // lblUserRole
            // 
            this.lblUserRole.AutoSize = true;
            this.lblUserRole.ForeColor = System.Drawing.Color.White;
            this.lblUserRole.Location = new System.Drawing.Point(128, 41);
            this.lblUserRole.Name = "lblUserRole";
            this.lblUserRole.Size = new System.Drawing.Size(166, 22);
            this.lblUserRole.TabIndex = 5;
            this.lblUserRole.Text = "Customer/Admin";
            // 
            // Username
            // 
            this.Username.AutoSize = true;
            this.Username.ForeColor = System.Drawing.Color.White;
            this.Username.Location = new System.Drawing.Point(128, 9);
            this.Username.Name = "Username";
            this.Username.Size = new System.Drawing.Size(101, 22);
            this.Username.TabIndex = 4;
            this.Username.Text = "Username";
            // 
            // lbRole
            // 
            this.lbRole.AutoSize = true;
            this.lbRole.ForeColor = System.Drawing.Color.White;
            this.lbRole.Location = new System.Drawing.Point(22, 41);
            this.lbRole.Name = "lbRole";
            this.lbRole.Size = new System.Drawing.Size(54, 22);
            this.lbRole.TabIndex = 3;
            this.lbRole.Text = "Role:";
            // 
            // Dashboard
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1184, 681);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Name = "Dashboard";
            this.Text = "Dashboard";

            this.panel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lbBookStore;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button bttnProfile;
        private System.Windows.Forms.Button bttnHome;
        private System.Windows.Forms.Label lbWelcome;
        private System.Windows.Forms.Label lbTime;
        private System.Windows.Forms.Label lblUserRole;
        private System.Windows.Forms.Label Username;
        private System.Windows.Forms.Label lbRole;
        private System.Windows.Forms.Timer timerTime;
        private System.Windows.Forms.Button bttnShoppingCart;
        private System.Windows.Forms.Button bttnInvoice;
    }
}