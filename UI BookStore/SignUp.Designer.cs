﻿namespace UI_BookStore
{
    partial class SignUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SignUp));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pbxSmall_Icon = new System.Windows.Forms.PictureBox();
            this.lbBookStore = new System.Windows.Forms.Label();
            this.pbBooks = new System.Windows.Forms.PictureBox();
            this.lbWelcome = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.lbAddress = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lbEmail = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.lbUsername = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lbPassword = new System.Windows.Forms.Label();
            this.txtConfirm = new System.Windows.Forms.TextBox();
            this.lbConfirm = new System.Windows.Forms.Label();
            this.bttnSignUp = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSmall_Icon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBooks)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightSeaGreen;
            this.panel1.Controls.Add(this.pbxSmall_Icon);
            this.panel1.Controls.Add(this.lbBookStore);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1184, 65);
            this.panel1.TabIndex = 1;
            // 
            // pbxSmall_Icon
            // 
            this.pbxSmall_Icon.Image = ((System.Drawing.Image)(resources.GetObject("pbxSmall_Icon.Image")));
            this.pbxSmall_Icon.Location = new System.Drawing.Point(12, 0);
            this.pbxSmall_Icon.Name = "pbxSmall_Icon";
            this.pbxSmall_Icon.Size = new System.Drawing.Size(70, 69);
            this.pbxSmall_Icon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxSmall_Icon.TabIndex = 3;
            this.pbxSmall_Icon.TabStop = false;
            // 
            // lbBookStore
            // 
            this.lbBookStore.AutoSize = true;
            this.lbBookStore.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbBookStore.ForeColor = System.Drawing.Color.White;
            this.lbBookStore.Location = new System.Drawing.Point(88, 22);
            this.lbBookStore.Name = "lbBookStore";
            this.lbBookStore.Size = new System.Drawing.Size(216, 28);
            this.lbBookStore.TabIndex = 1;
            this.lbBookStore.Text = "Online Book Store";
            // 
            // pbBooks
            // 
            this.pbBooks.Image = ((System.Drawing.Image)(resources.GetObject("pbBooks.Image")));
            this.pbBooks.Location = new System.Drawing.Point(74, 115);
            this.pbBooks.Name = "pbBooks";
            this.pbBooks.Size = new System.Drawing.Size(449, 425);
            this.pbBooks.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbBooks.TabIndex = 2;
            this.pbBooks.TabStop = false;
            // 
            // lbWelcome
            // 
            this.lbWelcome.AutoSize = true;
            this.lbWelcome.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbWelcome.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.lbWelcome.Location = new System.Drawing.Point(642, 85);
            this.lbWelcome.Name = "lbWelcome";
            this.lbWelcome.Size = new System.Drawing.Size(370, 28);
            this.lbWelcome.TabIndex = 3;
            this.lbWelcome.Text = "Welcome to Online Book Store!";
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbName.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.lbName.Location = new System.Drawing.Point(573, 139);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(71, 22);
            this.lbName.TabIndex = 4;
            this.lbName.Text = "Name:";
            // 
            // txtName
            // 
            this.txtName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtName.Location = new System.Drawing.Point(755, 134);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(305, 27);
            this.txtName.TabIndex = 5;
            // 
            // txtAddress
            // 
            this.txtAddress.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtAddress.Location = new System.Drawing.Point(755, 187);
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(305, 105);
            this.txtAddress.TabIndex = 7;
            // 
            // lbAddress
            // 
            this.lbAddress.AutoSize = true;
            this.lbAddress.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbAddress.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.lbAddress.Location = new System.Drawing.Point(573, 192);
            this.lbAddress.Name = "lbAddress";
            this.lbAddress.Size = new System.Drawing.Size(88, 22);
            this.lbAddress.TabIndex = 6;
            this.lbAddress.Text = "Address:";
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtEmail.Location = new System.Drawing.Point(755, 319);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(305, 27);
            this.txtEmail.TabIndex = 9;
            // 
            // lbEmail
            // 
            this.lbEmail.AutoSize = true;
            this.lbEmail.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbEmail.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.lbEmail.Location = new System.Drawing.Point(573, 324);
            this.lbEmail.Name = "lbEmail";
            this.lbEmail.Size = new System.Drawing.Size(61, 22);
            this.lbEmail.TabIndex = 8;
            this.lbEmail.Text = "Email:";
            // 
            // txtUsername
            // 
            this.txtUsername.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtUsername.Location = new System.Drawing.Point(755, 374);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(305, 27);
            this.txtUsername.TabIndex = 11;
            // 
            // lbUsername
            // 
            this.lbUsername.AutoSize = true;
            this.lbUsername.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbUsername.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.lbUsername.Location = new System.Drawing.Point(573, 379);
            this.lbUsername.Name = "lbUsername";
            this.lbUsername.Size = new System.Drawing.Size(106, 22);
            this.lbUsername.TabIndex = 10;
            this.lbUsername.Text = "Username:";
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtPassword.Location = new System.Drawing.Point(755, 429);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(305, 27);
            this.txtPassword.TabIndex = 13;
            // 
            // lbPassword
            // 
            this.lbPassword.AutoSize = true;
            this.lbPassword.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbPassword.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.lbPassword.Location = new System.Drawing.Point(573, 434);
            this.lbPassword.Name = "lbPassword";
            this.lbPassword.Size = new System.Drawing.Size(100, 22);
            this.lbPassword.TabIndex = 12;
            this.lbPassword.Text = "Password:";
            // 
            // txtConfirm
            // 
            this.txtConfirm.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtConfirm.Location = new System.Drawing.Point(755, 476);
            this.txtConfirm.Name = "txtConfirm";
            this.txtConfirm.Size = new System.Drawing.Size(305, 27);
            this.txtConfirm.TabIndex = 15;
            // 
            // lbConfirm
            // 
            this.lbConfirm.AutoSize = true;
            this.lbConfirm.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbConfirm.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.lbConfirm.Location = new System.Drawing.Point(573, 481);
            this.lbConfirm.Name = "lbConfirm";
            this.lbConfirm.Size = new System.Drawing.Size(176, 22);
            this.lbConfirm.TabIndex = 14;
            this.lbConfirm.Text = "Confirm Password:";
            // 
            // bttnSignUp
            // 
            this.bttnSignUp.BackColor = System.Drawing.Color.LightSeaGreen;
            this.bttnSignUp.FlatAppearance.BorderSize = 0;
            this.bttnSignUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnSignUp.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bttnSignUp.ForeColor = System.Drawing.Color.White;
            this.bttnSignUp.Location = new System.Drawing.Point(755, 549);
            this.bttnSignUp.Name = "bttnSignUp";
            this.bttnSignUp.Size = new System.Drawing.Size(151, 46);
            this.bttnSignUp.TabIndex = 16;
            this.bttnSignUp.Text = "Sign Up";
            this.bttnSignUp.UseVisualStyleBackColor = false;
            this.bttnSignUp.Click += new System.EventHandler(this.bttnSignUp_Click);
            // 
            // SignUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1184, 681);
            this.Controls.Add(this.bttnSignUp);
            this.Controls.Add(this.txtConfirm);
            this.Controls.Add(this.lbConfirm);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.lbPassword);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.lbUsername);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.lbEmail);
            this.Controls.Add(this.txtAddress);
            this.Controls.Add(this.lbAddress);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.lbName);
            this.Controls.Add(this.lbWelcome);
            this.Controls.Add(this.pbBooks);
            this.Controls.Add(this.panel1);
            this.Name = "SignUp";
            this.Text = "Sign Up";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSmall_Icon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBooks)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pbxSmall_Icon;
        private System.Windows.Forms.Label lbBookStore;
        private System.Windows.Forms.PictureBox pbBooks;
        private System.Windows.Forms.Label lbWelcome;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label lbAddress;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lbEmail;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label lbUsername;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lbPassword;
        private System.Windows.Forms.TextBox txtConfirm;
        private System.Windows.Forms.Label lbConfirm;
        private System.Windows.Forms.Button bttnSignUp;
    }
}