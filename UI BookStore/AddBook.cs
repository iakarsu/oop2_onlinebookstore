﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI_BookStore
{
    public partial class AddBook : Form
    {
        public AddBook()
        {
            InitializeComponent();
        }

        private void button_addcover_Click(object sender, EventArgs e)
        {
            string img_location;
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.ShowDialog();

            img_location = ofd.FileName;
            pictureBox_BOOKcover.ImageLocation = img_location;

            
        }

        private void button_AddBook_Click(object sender, EventArgs e)
        {
            //Veritabanı.kitap_adi = textBox_bookname.Text;
            //Veritabanı.kitap_yazari = textBox_author.Text;
            //Veritabanı.kitap_yayinci = textBox_publisher.Text;
            //Veritabanı.kitap_sayfa = textBox_page.Text;
            //Veritabanı.kitap_isbn = textBox_isbn.Text;

            this.Close();
        }
    }
}
