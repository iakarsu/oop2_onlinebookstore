﻿namespace AdminPage
{
    partial class Form_usercontrol
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_update = new System.Windows.Forms.Button();
            this.button_Block = new System.Windows.Forms.Button();
            this.button_Confirm = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // button_update
            // 
            this.button_update.BackColor = System.Drawing.Color.MediumTurquoise;
            this.button_update.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_update.FlatAppearance.BorderColor = System.Drawing.Color.LightSeaGreen;
            this.button_update.FlatAppearance.BorderSize = 0;
            this.button_update.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_update.Font = new System.Drawing.Font("Lemon/Milk", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_update.ForeColor = System.Drawing.Color.White;
            this.button_update.Location = new System.Drawing.Point(-3, 284);
            this.button_update.Name = "button_update";
            this.button_update.Size = new System.Drawing.Size(282, 121);
            this.button_update.TabIndex = 9;
            this.button_update.Text = "Update user info";
            this.button_update.UseVisualStyleBackColor = false;
            this.button_update.Click += new System.EventHandler(this.button_update_Click);
            // 
            // button_Block
            // 
            this.button_Block.BackColor = System.Drawing.Color.MediumTurquoise;
            this.button_Block.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_Block.FlatAppearance.BorderColor = System.Drawing.Color.LightSeaGreen;
            this.button_Block.FlatAppearance.BorderSize = 0;
            this.button_Block.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Block.Font = new System.Drawing.Font("Lemon/Milk", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Block.ForeColor = System.Drawing.Color.White;
            this.button_Block.Location = new System.Drawing.Point(-3, 168);
            this.button_Block.Name = "button_Block";
            this.button_Block.Size = new System.Drawing.Size(282, 121);
            this.button_Block.TabIndex = 8;
            this.button_Block.Text = "Block user";
            this.button_Block.UseVisualStyleBackColor = false;
            this.button_Block.Click += new System.EventHandler(this.button_Block_Click);
            // 
            // button_Confirm
            // 
            this.button_Confirm.BackColor = System.Drawing.Color.MediumTurquoise;
            this.button_Confirm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_Confirm.FlatAppearance.BorderColor = System.Drawing.Color.LightSeaGreen;
            this.button_Confirm.FlatAppearance.BorderSize = 0;
            this.button_Confirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Confirm.Font = new System.Drawing.Font("Lemon/Milk", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Confirm.ForeColor = System.Drawing.Color.White;
            this.button_Confirm.Location = new System.Drawing.Point(-3, 50);
            this.button_Confirm.Name = "button_Confirm";
            this.button_Confirm.Size = new System.Drawing.Size(282, 121);
            this.button_Confirm.TabIndex = 7;
            this.button_Confirm.Text = "confirm new user";
            this.button_Confirm.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkOrange;
            this.panel1.Location = new System.Drawing.Point(-3, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1200, 50);
            this.panel1.TabIndex = 6;
            // 
            // Form_usercontrol
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1035, 406);
            this.Controls.Add(this.button_update);
            this.Controls.Add(this.button_Block);
            this.Controls.Add(this.button_Confirm);
            this.Controls.Add(this.panel1);
            this.Name = "Form_usercontrol";
            this.Text = "Form_usercontrol";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button button_update;
        private System.Windows.Forms.Button button_Block;
        private System.Windows.Forms.Button button_Confirm;
        private System.Windows.Forms.Panel panel1;
    }
}