﻿namespace AdminPage
{
    partial class Form_addmagazine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_isbn = new System.Windows.Forms.Label();
            this.textBox_isbn = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label_HEADER = new System.Windows.Forms.Label();
            this.button_addcover = new System.Windows.Forms.Button();
            this.label_Cover = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label_price = new System.Windows.Forms.Label();
            this.label_page = new System.Windows.Forms.Label();
            this.label_author = new System.Windows.Forms.Label();
            this.label_name = new System.Windows.Forms.Label();
            this.textBox_price = new System.Windows.Forms.TextBox();
            this.textBox_page = new System.Windows.Forms.TextBox();
            this.textBox_author = new System.Windows.Forms.TextBox();
            this.textBox_name = new System.Windows.Forms.TextBox();
            this.button_add = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label_isbn
            // 
            this.label_isbn.AutoSize = true;
            this.label_isbn.Font = new System.Drawing.Font("Lemon/Milk", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_isbn.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.label_isbn.Location = new System.Drawing.Point(50, 159);
            this.label_isbn.Name = "label_isbn";
            this.label_isbn.Size = new System.Drawing.Size(53, 27);
            this.label_isbn.TabIndex = 29;
            this.label_isbn.Text = "ISBN";
            // 
            // textBox_isbn
            // 
            this.textBox_isbn.Font = new System.Drawing.Font("Lemon/Milk", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_isbn.Location = new System.Drawing.Point(109, 162);
            this.textBox_isbn.Multiline = true;
            this.textBox_isbn.Name = "textBox_isbn";
            this.textBox_isbn.Size = new System.Drawing.Size(213, 24);
            this.textBox_isbn.TabIndex = 28;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Orange;
            this.panel1.Controls.Add(this.label_HEADER);
            this.panel1.Location = new System.Drawing.Point(-2, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(805, 37);
            this.panel1.TabIndex = 27;
            // 
            // label_HEADER
            // 
            this.label_HEADER.AutoSize = true;
            this.label_HEADER.Font = new System.Drawing.Font("Lemon/Milk", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_HEADER.Location = new System.Drawing.Point(3, 4);
            this.label_HEADER.Name = "label_HEADER";
            this.label_HEADER.Size = new System.Drawing.Size(172, 30);
            this.label_HEADER.TabIndex = 13;
            this.label_HEADER.Text = "NEW MAGAZINE";
            // 
            // button_addcover
            // 
            this.button_addcover.Font = new System.Drawing.Font("Lemon/Milk", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_addcover.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.button_addcover.Location = new System.Drawing.Point(525, 219);
            this.button_addcover.Name = "button_addcover";
            this.button_addcover.Size = new System.Drawing.Size(172, 101);
            this.button_addcover.TabIndex = 26;
            this.button_addcover.Text = "Add Cover image";
            this.button_addcover.UseVisualStyleBackColor = true;
            this.button_addcover.Click += new System.EventHandler(this.button_addcover_Click);
            // 
            // label_Cover
            // 
            this.label_Cover.AutoSize = true;
            this.label_Cover.Font = new System.Drawing.Font("Lemon/Milk", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Cover.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.label_Cover.Location = new System.Drawing.Point(427, 55);
            this.label_Cover.Name = "label_Cover";
            this.label_Cover.Size = new System.Drawing.Size(76, 27);
            this.label_Cover.TabIndex = 25;
            this.label_Cover.Text = "Cover";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(525, 55);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(172, 153);
            this.pictureBox1.TabIndex = 24;
            this.pictureBox1.TabStop = false;
            // 
            // label_price
            // 
            this.label_price.AutoSize = true;
            this.label_price.Font = new System.Drawing.Font("Lemon/Milk", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_price.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.label_price.Location = new System.Drawing.Point(38, 129);
            this.label_price.Name = "label_price";
            this.label_price.Size = new System.Drawing.Size(65, 27);
            this.label_price.TabIndex = 23;
            this.label_price.Text = "Price";
            // 
            // label_page
            // 
            this.label_page.AutoSize = true;
            this.label_page.Font = new System.Drawing.Font("Lemon/Milk", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_page.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.label_page.Location = new System.Drawing.Point(43, 102);
            this.label_page.Name = "label_page";
            this.label_page.Size = new System.Drawing.Size(60, 27);
            this.label_page.TabIndex = 22;
            this.label_page.Text = "Page";
            // 
            // label_author
            // 
            this.label_author.AutoSize = true;
            this.label_author.Font = new System.Drawing.Font("Lemon/Milk", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_author.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.label_author.Location = new System.Drawing.Point(12, 69);
            this.label_author.Name = "label_author";
            this.label_author.Size = new System.Drawing.Size(91, 27);
            this.label_author.TabIndex = 21;
            this.label_author.Text = "Author";
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.Font = new System.Drawing.Font("Lemon/Milk", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_name.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.label_name.Location = new System.Drawing.Point(36, 42);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(67, 27);
            this.label_name.TabIndex = 20;
            this.label_name.Text = "Name";
            // 
            // textBox_price
            // 
            this.textBox_price.Font = new System.Drawing.Font("Lemon/Milk", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_price.Location = new System.Drawing.Point(109, 132);
            this.textBox_price.Multiline = true;
            this.textBox_price.Name = "textBox_price";
            this.textBox_price.Size = new System.Drawing.Size(213, 24);
            this.textBox_price.TabIndex = 19;
            // 
            // textBox_page
            // 
            this.textBox_page.Font = new System.Drawing.Font("Lemon/Milk", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_page.Location = new System.Drawing.Point(109, 102);
            this.textBox_page.Multiline = true;
            this.textBox_page.Name = "textBox_page";
            this.textBox_page.Size = new System.Drawing.Size(213, 24);
            this.textBox_page.TabIndex = 18;
            // 
            // textBox_author
            // 
            this.textBox_author.Font = new System.Drawing.Font("Lemon/Milk", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_author.Location = new System.Drawing.Point(109, 72);
            this.textBox_author.Multiline = true;
            this.textBox_author.Name = "textBox_author";
            this.textBox_author.Size = new System.Drawing.Size(213, 24);
            this.textBox_author.TabIndex = 17;
            // 
            // textBox_name
            // 
            this.textBox_name.Font = new System.Drawing.Font("Lemon/Milk", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_name.Location = new System.Drawing.Point(109, 42);
            this.textBox_name.Multiline = true;
            this.textBox_name.Name = "textBox_name";
            this.textBox_name.Size = new System.Drawing.Size(213, 24);
            this.textBox_name.TabIndex = 16;
            // 
            // button_add
            // 
            this.button_add.Font = new System.Drawing.Font("Lemon/Milk", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_add.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.button_add.Location = new System.Drawing.Point(317, 367);
            this.button_add.Name = "button_add";
            this.button_add.Size = new System.Drawing.Size(160, 71);
            this.button_add.TabIndex = 15;
            this.button_add.Text = "ADD MAGAZINE";
            this.button_add.UseVisualStyleBackColor = true;
            this.button_add.Click += new System.EventHandler(this.button_add_Click);
            // 
            // Form_addmagazine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label_isbn);
            this.Controls.Add(this.textBox_isbn);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button_addcover);
            this.Controls.Add(this.label_Cover);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label_price);
            this.Controls.Add(this.label_page);
            this.Controls.Add(this.label_author);
            this.Controls.Add(this.label_name);
            this.Controls.Add(this.textBox_price);
            this.Controls.Add(this.textBox_page);
            this.Controls.Add(this.textBox_author);
            this.Controls.Add(this.textBox_name);
            this.Controls.Add(this.button_add);
            this.Name = "Form_addmagazine";
            this.Text = "Form_addmagazine";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_isbn;
        private System.Windows.Forms.TextBox textBox_isbn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label_HEADER;
        private System.Windows.Forms.Button button_addcover;
        private System.Windows.Forms.Label label_Cover;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label_price;
        private System.Windows.Forms.Label label_page;
        private System.Windows.Forms.Label label_author;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.TextBox textBox_price;
        private System.Windows.Forms.TextBox textBox_page;
        private System.Windows.Forms.TextBox textBox_author;
        private System.Windows.Forms.TextBox textBox_name;
        private System.Windows.Forms.Button button_add;
    }
}