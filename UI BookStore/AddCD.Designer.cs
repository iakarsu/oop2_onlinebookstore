﻿namespace UI_BookStore
{
    partial class AddCD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddCD));
            this.label_price = new System.Windows.Forms.Label();
            this.textBox_CDprice = new System.Windows.Forms.TextBox();
            this.button_AddCD = new System.Windows.Forms.Button();
            this.button_CDcover = new System.Windows.Forms.Button();
            this.label_singer = new System.Windows.Forms.Label();
            this.label_desc = new System.Windows.Forms.Label();
            this.label_type = new System.Windows.Forms.Label();
            this.label_name = new System.Windows.Forms.Label();
            this.pictureBox_CDcover = new System.Windows.Forms.PictureBox();
            this.textBox_CDsinger = new System.Windows.Forms.TextBox();
            this.textBox_CDdesc = new System.Windows.Forms.TextBox();
            this.textBox_CDtype = new System.Windows.Forms.TextBox();
            this.textBox_CDname = new System.Windows.Forms.TextBox();
            this.label_sale = new System.Windows.Forms.Label();
            this.textBox_CDsale = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_CDcover)).BeginInit();
            this.SuspendLayout();
            // 
            // label_price
            // 
            this.label_price.AutoSize = true;
            this.label_price.BackColor = System.Drawing.Color.Transparent;
            this.label_price.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_price.Location = new System.Drawing.Point(76, 162);
            this.label_price.Name = "label_price";
            this.label_price.Size = new System.Drawing.Size(74, 29);
            this.label_price.TabIndex = 42;
            this.label_price.Text = "Price";
            // 
            // textBox_CDprice
            // 
            this.textBox_CDprice.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_CDprice.Location = new System.Drawing.Point(155, 159);
            this.textBox_CDprice.Name = "textBox_CDprice";
            this.textBox_CDprice.Size = new System.Drawing.Size(219, 35);
            this.textBox_CDprice.TabIndex = 41;
            // 
            // button_AddCD
            // 
            this.button_AddCD.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_AddCD.Location = new System.Drawing.Point(648, 368);
            this.button_AddCD.Name = "button_AddCD";
            this.button_AddCD.Size = new System.Drawing.Size(140, 70);
            this.button_AddCD.TabIndex = 40;
            this.button_AddCD.Text = "Add CD";
            this.button_AddCD.UseVisualStyleBackColor = true;
            this.button_AddCD.Click += new System.EventHandler(this.button_AddCD_Click);
            // 
            // button_CDcover
            // 
            this.button_CDcover.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_CDcover.Location = new System.Drawing.Point(500, 243);
            this.button_CDcover.Name = "button_CDcover";
            this.button_CDcover.Size = new System.Drawing.Size(223, 39);
            this.button_CDcover.TabIndex = 39;
            this.button_CDcover.Text = "Add Magazine Cover";
            this.button_CDcover.UseVisualStyleBackColor = true;
            this.button_CDcover.Click += new System.EventHandler(this.button_CDcover_Click);
            // 
            // label_singer
            // 
            this.label_singer.AutoSize = true;
            this.label_singer.BackColor = System.Drawing.Color.Transparent;
            this.label_singer.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_singer.Location = new System.Drawing.Point(59, 124);
            this.label_singer.Name = "label_singer";
            this.label_singer.Size = new System.Drawing.Size(90, 29);
            this.label_singer.TabIndex = 38;
            this.label_singer.Text = "Singer";
            // 
            // label_desc
            // 
            this.label_desc.AutoSize = true;
            this.label_desc.BackColor = System.Drawing.Color.Transparent;
            this.label_desc.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_desc.Location = new System.Drawing.Point(3, 243);
            this.label_desc.Name = "label_desc";
            this.label_desc.Size = new System.Drawing.Size(146, 29);
            this.label_desc.TabIndex = 36;
            this.label_desc.Text = "Description";
            // 
            // label_type
            // 
            this.label_type.AutoSize = true;
            this.label_type.BackColor = System.Drawing.Color.Transparent;
            this.label_type.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_type.Location = new System.Drawing.Point(77, 79);
            this.label_type.Name = "label_type";
            this.label_type.Size = new System.Drawing.Size(72, 29);
            this.label_type.TabIndex = 35;
            this.label_type.Text = "Type";
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.BackColor = System.Drawing.Color.Transparent;
            this.label_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_name.Location = new System.Drawing.Point(67, 38);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(82, 29);
            this.label_name.TabIndex = 34;
            this.label_name.Text = "Name";
            // 
            // pictureBox_CDcover
            // 
            this.pictureBox_CDcover.Location = new System.Drawing.Point(500, 38);
            this.pictureBox_CDcover.Name = "pictureBox_CDcover";
            this.pictureBox_CDcover.Size = new System.Drawing.Size(223, 199);
            this.pictureBox_CDcover.TabIndex = 33;
            this.pictureBox_CDcover.TabStop = false;
            // 
            // textBox_CDsinger
            // 
            this.textBox_CDsinger.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_CDsinger.Location = new System.Drawing.Point(155, 118);
            this.textBox_CDsinger.Name = "textBox_CDsinger";
            this.textBox_CDsinger.Size = new System.Drawing.Size(219, 35);
            this.textBox_CDsinger.TabIndex = 32;
            // 
            // textBox_CDdesc
            // 
            this.textBox_CDdesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_CDdesc.Location = new System.Drawing.Point(155, 243);
            this.textBox_CDdesc.Multiline = true;
            this.textBox_CDdesc.Name = "textBox_CDdesc";
            this.textBox_CDdesc.Size = new System.Drawing.Size(219, 114);
            this.textBox_CDdesc.TabIndex = 30;
            // 
            // textBox_CDtype
            // 
            this.textBox_CDtype.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_CDtype.Location = new System.Drawing.Point(154, 76);
            this.textBox_CDtype.Name = "textBox_CDtype";
            this.textBox_CDtype.Size = new System.Drawing.Size(219, 35);
            this.textBox_CDtype.TabIndex = 29;
            // 
            // textBox_CDname
            // 
            this.textBox_CDname.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_CDname.Location = new System.Drawing.Point(154, 35);
            this.textBox_CDname.Name = "textBox_CDname";
            this.textBox_CDname.Size = new System.Drawing.Size(219, 35);
            this.textBox_CDname.TabIndex = 28;
            // 
            // label_sale
            // 
            this.label_sale.AutoSize = true;
            this.label_sale.BackColor = System.Drawing.Color.Transparent;
            this.label_sale.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_sale.Location = new System.Drawing.Point(75, 205);
            this.label_sale.Name = "label_sale";
            this.label_sale.Size = new System.Drawing.Size(66, 29);
            this.label_sale.TabIndex = 44;
            this.label_sale.Text = "Sale";
            // 
            // textBox_CDsale
            // 
            this.textBox_CDsale.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_CDsale.Location = new System.Drawing.Point(154, 202);
            this.textBox_CDsale.Name = "textBox_CDsale";
            this.textBox_CDsale.Size = new System.Drawing.Size(219, 35);
            this.textBox_CDsale.TabIndex = 43;
            // 
            // AddCD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label_sale);
            this.Controls.Add(this.textBox_CDsale);
            this.Controls.Add(this.label_price);
            this.Controls.Add(this.textBox_CDprice);
            this.Controls.Add(this.button_AddCD);
            this.Controls.Add(this.button_CDcover);
            this.Controls.Add(this.label_singer);
            this.Controls.Add(this.label_desc);
            this.Controls.Add(this.label_type);
            this.Controls.Add(this.label_name);
            this.Controls.Add(this.pictureBox_CDcover);
            this.Controls.Add(this.textBox_CDsinger);
            this.Controls.Add(this.textBox_CDdesc);
            this.Controls.Add(this.textBox_CDtype);
            this.Controls.Add(this.textBox_CDname);
            this.Name = "AddCD";
            this.Text = "AddCD";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_CDcover)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_price;
        private System.Windows.Forms.TextBox textBox_CDprice;
        private System.Windows.Forms.Button button_AddCD;
        private System.Windows.Forms.Button button_CDcover;
        private System.Windows.Forms.Label label_singer;
        private System.Windows.Forms.Label label_desc;
        private System.Windows.Forms.Label label_type;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.PictureBox pictureBox_CDcover;
        private System.Windows.Forms.TextBox textBox_CDsinger;
        private System.Windows.Forms.TextBox textBox_CDdesc;
        private System.Windows.Forms.TextBox textBox_CDtype;
        private System.Windows.Forms.TextBox textBox_CDname;
        private System.Windows.Forms.Label label_sale;
        private System.Windows.Forms.TextBox textBox_CDsale;
    }
}