﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI_BookStore
{
    public partial class SignUp : Form
    {
        public SignUp()
        {
            InitializeComponent();
        }

        private void bttnSignUp_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(txtName.Text)|| string.IsNullOrWhiteSpace(txtAddress.Text)|| string.IsNullOrWhiteSpace(txtEmail.Text)|| string.IsNullOrWhiteSpace(txtUsername.Text)|| string.IsNullOrWhiteSpace(txtPassword.Text)|| string.IsNullOrWhiteSpace(txtConfirm.Text))
            {
                MessageBox.Show("There are empty fields!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if(!IsValidEmail(txtEmail.Text))
            {
                MessageBox.Show("Email is not correct!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if(txtPassword.Text!=txtConfirm.Text)
            {
                MessageBox.Show("Passwords are not matching!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            else
            {
                //connect to DB
            }
        }

        //Check if email is correct
        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
