﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI_BookStore
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {

            //Database öncesi
            AdminPage admin = new AdminPage();
            admin.ShowDialog();


            Dashboard dashboard = new Dashboard();
            dashboard.ShowDialog();
        }

        private void SignUpLink(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SignUp sign = new SignUp();
            this.Hide();
            if (sign.ShowDialog() == DialogResult.OK)
                MessageBox.Show("Sign up successfully. You can use your new account.", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Show();
        }

      
    }
}
