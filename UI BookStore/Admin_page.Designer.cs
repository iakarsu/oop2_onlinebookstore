﻿namespace AdminPage
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_addbook = new System.Windows.Forms.Button();
            this.button_addmagazine = new System.Windows.Forms.Button();
            this.button_musiccd = new System.Windows.Forms.Button();
            this.button_userop = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button_adminprofile = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkOrange;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1200, 50);
            this.panel1.TabIndex = 0;
            // 
            // button_addbook
            // 
            this.button_addbook.BackColor = System.Drawing.Color.MediumTurquoise;
            this.button_addbook.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_addbook.FlatAppearance.BorderColor = System.Drawing.Color.LightSeaGreen;
            this.button_addbook.FlatAppearance.BorderSize = 0;
            this.button_addbook.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_addbook.Font = new System.Drawing.Font("Lemon/Milk", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_addbook.ForeColor = System.Drawing.Color.White;
            this.button_addbook.Location = new System.Drawing.Point(0, 50);
            this.button_addbook.Name = "button_addbook";
            this.button_addbook.Size = new System.Drawing.Size(282, 89);
            this.button_addbook.TabIndex = 2;
            this.button_addbook.Text = "add new book";
            this.button_addbook.UseVisualStyleBackColor = false;
            this.button_addbook.Click += new System.EventHandler(this.button_addbook_Click);
            // 
            // button_addmagazine
            // 
            this.button_addmagazine.BackColor = System.Drawing.Color.MediumTurquoise;
            this.button_addmagazine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_addmagazine.FlatAppearance.BorderColor = System.Drawing.Color.LightSeaGreen;
            this.button_addmagazine.FlatAppearance.BorderSize = 0;
            this.button_addmagazine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_addmagazine.Font = new System.Drawing.Font("Lemon/Milk", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_addmagazine.ForeColor = System.Drawing.Color.White;
            this.button_addmagazine.Location = new System.Drawing.Point(0, 139);
            this.button_addmagazine.Name = "button_addmagazine";
            this.button_addmagazine.Size = new System.Drawing.Size(282, 89);
            this.button_addmagazine.TabIndex = 3;
            this.button_addmagazine.Text = "add new magazine";
            this.button_addmagazine.UseVisualStyleBackColor = false;
            this.button_addmagazine.Click += new System.EventHandler(this.button_addmagazine_Click);
            // 
            // button_musiccd
            // 
            this.button_musiccd.BackColor = System.Drawing.Color.MediumTurquoise;
            this.button_musiccd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_musiccd.FlatAppearance.BorderColor = System.Drawing.Color.LightSeaGreen;
            this.button_musiccd.FlatAppearance.BorderSize = 0;
            this.button_musiccd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_musiccd.Font = new System.Drawing.Font("Lemon/Milk", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_musiccd.ForeColor = System.Drawing.Color.White;
            this.button_musiccd.Location = new System.Drawing.Point(0, 228);
            this.button_musiccd.Name = "button_musiccd";
            this.button_musiccd.Size = new System.Drawing.Size(282, 89);
            this.button_musiccd.TabIndex = 4;
            this.button_musiccd.Text = "Add new music cd";
            this.button_musiccd.UseVisualStyleBackColor = false;
            this.button_musiccd.Click += new System.EventHandler(this.button_musiccd_Click);
            // 
            // button_userop
            // 
            this.button_userop.BackColor = System.Drawing.Color.MediumTurquoise;
            this.button_userop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_userop.FlatAppearance.BorderColor = System.Drawing.Color.LightSeaGreen;
            this.button_userop.FlatAppearance.BorderSize = 0;
            this.button_userop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_userop.Font = new System.Drawing.Font("Lemon/Milk", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_userop.ForeColor = System.Drawing.Color.White;
            this.button_userop.Location = new System.Drawing.Point(0, 317);
            this.button_userop.Name = "button_userop";
            this.button_userop.Size = new System.Drawing.Size(282, 89);
            this.button_userop.TabIndex = 5;
            this.button_userop.Text = "User operations";
            this.button_userop.UseVisualStyleBackColor = false;
            this.button_userop.Click += new System.EventHandler(this.button_userop_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lemon/Milk", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(443, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(578, 68);
            this.label1.TabIndex = 6;
            this.label1.Text = "Welcome USER_NAME!";
            // 
            // button_adminprofile
            // 
            this.button_adminprofile.BackColor = System.Drawing.Color.MediumTurquoise;
            this.button_adminprofile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_adminprofile.FlatAppearance.BorderColor = System.Drawing.Color.LightSeaGreen;
            this.button_adminprofile.FlatAppearance.BorderSize = 0;
            this.button_adminprofile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_adminprofile.Font = new System.Drawing.Font("Lemon/Milk", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_adminprofile.ForeColor = System.Drawing.Color.White;
            this.button_adminprofile.Location = new System.Drawing.Point(582, 228);
            this.button_adminprofile.Name = "button_adminprofile";
            this.button_adminprofile.Size = new System.Drawing.Size(282, 89);
            this.button_adminprofile.TabIndex = 7;
            this.button_adminprofile.Text = "My profile";
            this.button_adminprofile.UseVisualStyleBackColor = false;
            this.button_adminprofile.Click += new System.EventHandler(this.button_adminprofile_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1180, 403);
            this.Controls.Add(this.button_adminprofile);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_userop);
            this.Controls.Add(this.button_musiccd);
            this.Controls.Add(this.button_addmagazine);
            this.Controls.Add(this.button_addbook);
            this.Controls.Add(this.panel1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button_addbook;
        private System.Windows.Forms.Button button_addmagazine;
        private System.Windows.Forms.Button button_musiccd;
        private System.Windows.Forms.Button button_userop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_adminprofile;
    }
}

