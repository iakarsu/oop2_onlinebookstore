﻿namespace UI_BookStore
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pbSmall_Icon = new System.Windows.Forms.PictureBox();
            this.lbBookStore = new System.Windows.Forms.Label();
            this.lbLogin = new System.Windows.Forms.Label();
            this.pbBigIcon = new System.Windows.Forms.PictureBox();
            this.lbUsername = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.lbPassword = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.bttnLogin = new System.Windows.Forms.Button();
            this.lbCopyright = new System.Windows.Forms.Label();
            this.llbSignUp = new System.Windows.Forms.LinkLabel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSmall_Icon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBigIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightSeaGreen;
            this.panel1.Controls.Add(this.pbSmall_Icon);
            this.panel1.Controls.Add(this.lbBookStore);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1184, 65);
            this.panel1.TabIndex = 0;
            // 
            // pbSmall_Icon
            // 
            this.pbSmall_Icon.Image = ((System.Drawing.Image)(resources.GetObject("pbSmall_Icon.Image")));
            this.pbSmall_Icon.Location = new System.Drawing.Point(12, 0);
            this.pbSmall_Icon.Name = "pbSmall_Icon";
            this.pbSmall_Icon.Size = new System.Drawing.Size(70, 69);
            this.pbSmall_Icon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbSmall_Icon.TabIndex = 3;
            this.pbSmall_Icon.TabStop = false;
            // 
            // lbBookStore
            // 
            this.lbBookStore.AutoSize = true;
            this.lbBookStore.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbBookStore.ForeColor = System.Drawing.Color.White;
            this.lbBookStore.Location = new System.Drawing.Point(88, 22);
            this.lbBookStore.Name = "lbBookStore";
            this.lbBookStore.Size = new System.Drawing.Size(216, 28);
            this.lbBookStore.TabIndex = 1;
            this.lbBookStore.Text = "Online Book Store";
            // 
            // lbLogin
            // 
            this.lbLogin.AutoSize = true;
            this.lbLogin.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbLogin.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.lbLogin.Location = new System.Drawing.Point(487, 295);
            this.lbLogin.Name = "lbLogin";
            this.lbLogin.Size = new System.Drawing.Size(215, 28);
            this.lbLogin.TabIndex = 2;
            this.lbLogin.Text = "Please Log in First";
            // 
            // pbBigIcon
            // 
            this.pbBigIcon.Image = ((System.Drawing.Image)(resources.GetObject("pbBigIcon.Image")));
            this.pbBigIcon.Location = new System.Drawing.Point(502, 110);
            this.pbBigIcon.Name = "pbBigIcon";
            this.pbBigIcon.Size = new System.Drawing.Size(180, 161);
            this.pbBigIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbBigIcon.TabIndex = 2;
            this.pbBigIcon.TabStop = false;
            // 
            // lbUsername
            // 
            this.lbUsername.AutoSize = true;
            this.lbUsername.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.lbUsername.Location = new System.Drawing.Point(436, 348);
            this.lbUsername.Name = "lbUsername";
            this.lbUsername.Size = new System.Drawing.Size(106, 22);
            this.lbUsername.TabIndex = 3;
            this.lbUsername.Text = "Username:";
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(440, 373);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(310, 31);
            this.txtUsername.TabIndex = 4;
            // 
            // lbPassword
            // 
            this.lbPassword.AutoSize = true;
            this.lbPassword.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.lbPassword.Location = new System.Drawing.Point(436, 427);
            this.lbPassword.Name = "lbPassword";
            this.lbPassword.Size = new System.Drawing.Size(100, 22);
            this.lbPassword.TabIndex = 5;
            this.lbPassword.Text = "Password:";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(440, 452);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(310, 31);
            this.txtPassword.TabIndex = 6;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // bttnLogin
            // 
            this.bttnLogin.BackColor = System.Drawing.Color.LightSeaGreen;
            this.bttnLogin.FlatAppearance.BorderSize = 0;
            this.bttnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnLogin.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bttnLogin.ForeColor = System.Drawing.Color.White;
            this.bttnLogin.Location = new System.Drawing.Point(440, 521);
            this.bttnLogin.Name = "bttnLogin";
            this.bttnLogin.Size = new System.Drawing.Size(310, 40);
            this.bttnLogin.TabIndex = 7;
            this.bttnLogin.Text = "Log In";
            this.bttnLogin.UseVisualStyleBackColor = false;
            this.bttnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // lbCopyright
            // 
            this.lbCopyright.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbCopyright.AutoSize = true;
            this.lbCopyright.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbCopyright.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.lbCopyright.Location = new System.Drawing.Point(299, 655);
            this.lbCopyright.Name = "lbCopyright";
            this.lbCopyright.Size = new System.Drawing.Size(587, 17);
            this.lbCopyright.TabIndex = 8;
            this.lbCopyright.Text = "Copyrights © 2019. All rights reserved by Cenkay Basaran, Irfan Akarsu, Zhanara K" +
    "olbaeva. ";
            // 
            // llbSignUp
            // 
            this.llbSignUp.AutoSize = true;
            this.llbSignUp.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.llbSignUp.LinkColor = System.Drawing.Color.LightSeaGreen;
            this.llbSignUp.Location = new System.Drawing.Point(554, 585);
            this.llbSignUp.Name = "llbSignUp";
            this.llbSignUp.Size = new System.Drawing.Size(77, 22);
            this.llbSignUp.TabIndex = 10;
            this.llbSignUp.TabStop = true;
            this.llbSignUp.Text = "Sign Up";
            this.llbSignUp.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.SignUpLink);
            // 
            // LoginForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1184, 681);
            this.Controls.Add(this.llbSignUp);
            this.Controls.Add(this.lbCopyright);
            this.Controls.Add(this.bttnLogin);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.lbPassword);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.lbUsername);
            this.Controls.Add(this.pbBigIcon);
            this.Controls.Add(this.lbLogin);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Name = "LoginForm";
            this.Text = "Login ";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSmall_Icon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBigIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbBookStore;
        private System.Windows.Forms.Label lbLogin;
        private System.Windows.Forms.PictureBox pbSmall_Icon;
        private System.Windows.Forms.PictureBox pbBigIcon;
        private System.Windows.Forms.Label lbUsername;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label lbPassword;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Button bttnLogin;
        private System.Windows.Forms.Label lbCopyright;
        private System.Windows.Forms.LinkLabel llbSignUp;
    }
}

