﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AdminPage
{
    public partial class Form_addmagazine : Form
    {
        public Form_addmagazine()
        {
            InitializeComponent();
        }

        private void button_addcover_Click(object sender, EventArgs e)
        {
            string imagelocation;
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "JPG files(*.jpg)|*.jpg| PNG files (*.png)|*.png| All files (*.*)|*.*";

            if (open.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                imagelocation = open.FileName;
                pictureBox1.ImageLocation = imagelocation;
            }
        }

        private void button_add_Click(object sender, EventArgs e)
        {
            //dergi class'ından yeni bir dergi oluşturulacak eğer data base'de varsa aynısı messagebox.show ile hata gösterilecek
            pictureBox1.ImageLocation = null;
            textBox_author.Text = "";
            textBox_isbn.Text = "";
            textBox_name.Text = "";
            textBox_page.Text = "";
            textBox_price.Text = "";
            this.Close();
        }
    }
}
