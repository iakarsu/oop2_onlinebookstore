﻿namespace UI_BookStore
{
    partial class AddMagazine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddMagazine));
            this.button_AddMAG = new System.Windows.Forms.Button();
            this.button_addcover = new System.Windows.Forms.Button();
            this.label_issue = new System.Windows.Forms.Label();
            this.label_sale = new System.Windows.Forms.Label();
            this.label_desc = new System.Windows.Forms.Label();
            this.label_type = new System.Windows.Forms.Label();
            this.label_name = new System.Windows.Forms.Label();
            this.pictureBox_MAGcover = new System.Windows.Forms.PictureBox();
            this.textBox_MAGissue = new System.Windows.Forms.TextBox();
            this.textBox_MAGsale = new System.Windows.Forms.TextBox();
            this.textBox_MAGdesc = new System.Windows.Forms.TextBox();
            this.textBox_MAGtype = new System.Windows.Forms.TextBox();
            this.textBox_MAGname = new System.Windows.Forms.TextBox();
            this.label_price = new System.Windows.Forms.Label();
            this.textBox_MAGprice = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_MAGcover)).BeginInit();
            this.SuspendLayout();
            // 
            // button_AddMAG
            // 
            this.button_AddMAG.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_AddMAG.Location = new System.Drawing.Point(648, 368);
            this.button_AddMAG.Name = "button_AddMAG";
            this.button_AddMAG.Size = new System.Drawing.Size(140, 70);
            this.button_AddMAG.TabIndex = 25;
            this.button_AddMAG.Text = "Add Magazine";
            this.button_AddMAG.UseVisualStyleBackColor = true;
            this.button_AddMAG.Click += new System.EventHandler(this.button_AddMAG_Click);
            // 
            // button_addcover
            // 
            this.button_addcover.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_addcover.Location = new System.Drawing.Point(482, 245);
            this.button_addcover.Name = "button_addcover";
            this.button_addcover.Size = new System.Drawing.Size(246, 74);
            this.button_addcover.TabIndex = 24;
            this.button_addcover.Text = "Add Magazine Cover";
            this.button_addcover.UseVisualStyleBackColor = true;
            this.button_addcover.Click += new System.EventHandler(this.button_addcover_Click);
            // 
            // label_issue
            // 
            this.label_issue.AutoSize = true;
            this.label_issue.BackColor = System.Drawing.Color.Transparent;
            this.label_issue.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_issue.Location = new System.Drawing.Point(86, 153);
            this.label_issue.Name = "label_issue";
            this.label_issue.Size = new System.Drawing.Size(59, 24);
            this.label_issue.TabIndex = 23;
            this.label_issue.Text = "Issue";
            // 
            // label_sale
            // 
            this.label_sale.AutoSize = true;
            this.label_sale.BackColor = System.Drawing.Color.Transparent;
            this.label_sale.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_sale.Location = new System.Drawing.Point(87, 193);
            this.label_sale.Name = "label_sale";
            this.label_sale.Size = new System.Drawing.Size(51, 24);
            this.label_sale.TabIndex = 22;
            this.label_sale.Text = "Sale";
            // 
            // label_desc
            // 
            this.label_desc.AutoSize = true;
            this.label_desc.BackColor = System.Drawing.Color.Transparent;
            this.label_desc.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_desc.Location = new System.Drawing.Point(27, 230);
            this.label_desc.Name = "label_desc";
            this.label_desc.Size = new System.Drawing.Size(115, 24);
            this.label_desc.TabIndex = 21;
            this.label_desc.Text = "Description";
            // 
            // label_type
            // 
            this.label_type.AutoSize = true;
            this.label_type.BackColor = System.Drawing.Color.Transparent;
            this.label_type.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_type.Location = new System.Drawing.Point(88, 83);
            this.label_type.Name = "label_type";
            this.label_type.Size = new System.Drawing.Size(57, 24);
            this.label_type.TabIndex = 20;
            this.label_type.Text = "Type";
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.BackColor = System.Drawing.Color.Transparent;
            this.label_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_name.Location = new System.Drawing.Point(80, 46);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(65, 24);
            this.label_name.TabIndex = 19;
            this.label_name.Text = "Name";
            // 
            // pictureBox_MAGcover
            // 
            this.pictureBox_MAGcover.Location = new System.Drawing.Point(482, 39);
            this.pictureBox_MAGcover.Name = "pictureBox_MAGcover";
            this.pictureBox_MAGcover.Size = new System.Drawing.Size(246, 200);
            this.pictureBox_MAGcover.TabIndex = 18;
            this.pictureBox_MAGcover.TabStop = false;
            // 
            // textBox_MAGissue
            // 
            this.textBox_MAGissue.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_MAGissue.Location = new System.Drawing.Point(148, 149);
            this.textBox_MAGissue.Name = "textBox_MAGissue";
            this.textBox_MAGissue.Size = new System.Drawing.Size(219, 35);
            this.textBox_MAGissue.TabIndex = 17;
            // 
            // textBox_MAGsale
            // 
            this.textBox_MAGsale.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_MAGsale.Location = new System.Drawing.Point(148, 186);
            this.textBox_MAGsale.Name = "textBox_MAGsale";
            this.textBox_MAGsale.Size = new System.Drawing.Size(219, 35);
            this.textBox_MAGsale.TabIndex = 16;
            // 
            // textBox_MAGdesc
            // 
            this.textBox_MAGdesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_MAGdesc.Location = new System.Drawing.Point(148, 223);
            this.textBox_MAGdesc.Multiline = true;
            this.textBox_MAGdesc.Name = "textBox_MAGdesc";
            this.textBox_MAGdesc.Size = new System.Drawing.Size(219, 108);
            this.textBox_MAGdesc.TabIndex = 15;
            // 
            // textBox_MAGtype
            // 
            this.textBox_MAGtype.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_MAGtype.Location = new System.Drawing.Point(148, 76);
            this.textBox_MAGtype.Name = "textBox_MAGtype";
            this.textBox_MAGtype.Size = new System.Drawing.Size(219, 35);
            this.textBox_MAGtype.TabIndex = 14;
            // 
            // textBox_MAGname
            // 
            this.textBox_MAGname.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_MAGname.Location = new System.Drawing.Point(148, 39);
            this.textBox_MAGname.Name = "textBox_MAGname";
            this.textBox_MAGname.Size = new System.Drawing.Size(219, 35);
            this.textBox_MAGname.TabIndex = 13;
            // 
            // label_price
            // 
            this.label_price.AutoSize = true;
            this.label_price.BackColor = System.Drawing.Color.Transparent;
            this.label_price.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_price.Location = new System.Drawing.Point(87, 119);
            this.label_price.Name = "label_price";
            this.label_price.Size = new System.Drawing.Size(58, 24);
            this.label_price.TabIndex = 27;
            this.label_price.Text = "Price";
            // 
            // textBox_MAGprice
            // 
            this.textBox_MAGprice.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_MAGprice.Location = new System.Drawing.Point(148, 112);
            this.textBox_MAGprice.Name = "textBox_MAGprice";
            this.textBox_MAGprice.Size = new System.Drawing.Size(219, 35);
            this.textBox_MAGprice.TabIndex = 26;
            // 
            // AddMagazine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label_price);
            this.Controls.Add(this.textBox_MAGprice);
            this.Controls.Add(this.button_AddMAG);
            this.Controls.Add(this.button_addcover);
            this.Controls.Add(this.label_issue);
            this.Controls.Add(this.label_sale);
            this.Controls.Add(this.label_desc);
            this.Controls.Add(this.label_type);
            this.Controls.Add(this.label_name);
            this.Controls.Add(this.pictureBox_MAGcover);
            this.Controls.Add(this.textBox_MAGissue);
            this.Controls.Add(this.textBox_MAGsale);
            this.Controls.Add(this.textBox_MAGdesc);
            this.Controls.Add(this.textBox_MAGtype);
            this.Controls.Add(this.textBox_MAGname);
            this.Name = "AddMagazine";
            this.Text = "AddMagazine";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_MAGcover)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_AddMAG;
        private System.Windows.Forms.Button button_addcover;
        private System.Windows.Forms.Label label_issue;
        private System.Windows.Forms.Label label_sale;
        private System.Windows.Forms.Label label_desc;
        private System.Windows.Forms.Label label_type;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.PictureBox pictureBox_MAGcover;
        private System.Windows.Forms.TextBox textBox_MAGissue;
        private System.Windows.Forms.TextBox textBox_MAGsale;
        private System.Windows.Forms.TextBox textBox_MAGdesc;
        private System.Windows.Forms.TextBox textBox_MAGtype;
        private System.Windows.Forms.TextBox textBox_MAGname;
        private System.Windows.Forms.Label label_price;
        private System.Windows.Forms.TextBox textBox_MAGprice;
    }
}